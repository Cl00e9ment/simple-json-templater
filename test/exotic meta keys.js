const { testInvalidTemplate } = require('./test');

describe('exotic meta keys', () => {
	testInvalidTemplate('simple', {
		$abc: 123,
	}, {});

	testInvalidTemplate('single character', {
		$: 123,
	}, {});
});

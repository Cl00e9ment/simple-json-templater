const { testTemplate, testInvalidTemplate } = require('./test');

describe('$replaceByVar', () => {
	testTemplate('simple', {
		$replaceByVariable: 'VAR',
	}, {
		VAR: 'abc',
	}, 'abc');

	testTemplate('simple undefined', {
		$replaceByVariable: 'VAR',
	}, {}, undefined);

	testInvalidTemplate('simple invalid', {
		$replaceByVariable: 123,
	}, {});

	testTemplate('nested', {
		xyz: [
			{
				$replaceByVariable: 'VAR',
			},
		],
	}, {
		VAR: 'abc'
	}, {
		xyz: [
			'abc',
		],
	});
	
	testTemplate('nested undefined', {
		xyz: [
			{
				$replaceByVariable: 'VAR',
			},
		],
	}, {}, {
		xyz: [
			undefined,
		],
	});
	
	testInvalidTemplate('nested invalid', {
		xyz: [
			{
				$replaceByVariable: 123,
			},
		],
	}, {});
});

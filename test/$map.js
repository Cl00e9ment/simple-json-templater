const { testTemplate, testInvalidTemplate } = require('./test');

describe('$map', () => {
	testInvalidTemplate('$map must be of type string', {
		$map: 123,
	}, {});

	testTemplate('simple', {
		'$map': 'var1',
		'$case:abc': 123,
		'$case:def': 456,
	}, {
		var1: 'def',
	}, 456);

	testTemplate('simple default', {
		'$map': 'var1',
		'$case:abc': 123,
		'$case:def': 465,
		'$default': 789,
	}, {}, 789);

	testTemplate('simple missing default', {
		'$map': 'var1',
		'$case:abc': 123,
		'$case:def': 465,
	}, {}, undefined);

	testTemplate('nested', {
		uvw: [
			{
				'$map': 'var1',
				'$case:abc': 123,
				'$case:def': 456,
			}
		],
		xyz: {
			'$map': 'var1',
			'$case:abc': 456,
			'$case:def': 123,
		}
	}, {
		var1: 'abc',
	}, {
		uvw: [ 123 ],
		xyz: 456,
	});

	testTemplate('nested default', {
		uvw: [
			{
				'$map': 'var1',
				'$case:abc': 123,
				'$case:def': 456,
				'$default': 789,
			}
		],
		xyz: {
			'$map': 'var1',
			'$case:abc': 456,
			'$case:def': 123,
			'$default': 789,
		}
	}, {}, {
		uvw: [ 789 ],
		xyz: 789,
	});

	testTemplate('nested missing default', {
		uvw: [
			{
				'$map': 'var1',
				'$case:abc': 123,
				'$case:def': 456,
			}
		],
		xyz: {
			'$map': 'var1',
			'$case:abc': 456,
			'$case:def': 123,
		}
	}, {}, {
		uvw: [ undefined ],
		xyz: undefined,
	});
});

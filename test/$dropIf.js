const { testTemplate } = require('./test');

describe('$dropIf', () => {
	testTemplate('positive', {
		$dropIf: 'VAR is abc',
		xyz: 123,
	}, {
		VAR: 'abc',
	}, undefined);

	testTemplate('negative', {
		$dropIf: 'VAR is abc',
		xyz: 123,
	}, {}, {
		xyz: 123,
	});

	testTemplate('nested positive', {
		abc: [
			{
				$dropIf: 'VAR is abc',
				xyz: 123,
			},
		],
	}, {
		VAR: 'abc',
	}, {
		abc: [],
	});

	testTemplate('nested negative', {
		abc: [
			{
				$dropIf: 'VAR is abc',
				xyz: 123,
			},
		],
		def: {
			$keepIf: 'VAR is abc',
			xyz: 123,
		},
	}, {}, {
		abc: [
			{
				xyz: 123,
			},
		],
	});
});

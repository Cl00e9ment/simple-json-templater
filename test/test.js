const chai = require('chai');
const chaiMembersDeep = require('chai-members-deep');
const { parse } = require('../dist/index');

chai.use(chaiMembersDeep);
const expect = chai.expect;

function testTemplate(testName, template, variables, expected) {
	it(testName, () => {
		expect(parse(template, variables)).to.identically.deep.equal(expected);
	});
}

function testInvalidTemplate(testName, template, variables) {
	it(testName, () => {
		expect(() => {
			parse(template, variables);
		}).to.Throw();
	});
}

module.exports = { testTemplate, testInvalidTemplate };

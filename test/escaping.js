const { testTemplate } = require('./test');

describe('escaping', () => {
	testTemplate('simple', {
		$$abc: 123,
	}, {}, {
		$abc: 123,
	});

	testTemplate('nested', {
		xyz: [
			{
				$$abc: 123,
			},
		],
	}, {}, {
		xyz: [
			{
				$abc: 123,
			},
		],
	});
});

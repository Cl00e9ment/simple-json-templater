const { testInvalidTemplate } = require('./test');

describe('basic checks', () => {
	testInvalidTemplate('template is mendatory', undefined, {});
	testInvalidTemplate('template must be an object', 'abc', {});
	
	const template = {};
	template.abc = template;
	testInvalidTemplate('template can\'t contains a circular reference', template, {});
	
	testInvalidTemplate('variables must be an object', {}, 'abc');
	testInvalidTemplate('variables is of type { [key: string]: string }', {}, { abc: 123 });
});

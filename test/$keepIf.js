const { testTemplate } = require('./test');

describe('$keepIf', () => {
	testTemplate('positive is', {
		$keepIf: 'VAR is abc',
		xyz: 123,
	}, {
		VAR: 'abc'
	}, {
		xyz: 123,
	});

	testTemplate('negative is', {
		$keepIf: 'VAR is abc',
		xyz: 123,
	}, {
		VAR: 'xyz'
	}, undefined);

	testTemplate('negative is (undefined)', {
		$keepIf: 'VAR is abc',
		xyz: 123,
	}, {}, undefined);

	testTemplate('positive isn\'t', {
		$keepIf: 'VAR isn\'t abc',
		xyz: 123,
	}, {
		VAR: 'xyz'
	}, {
		xyz: 123,
	});

	testTemplate('positive isn\'t (undefined)', {
		$keepIf: 'VAR isn\'t abc',
		xyz: 123,
	}, {}, {
		xyz: 123,
	});

	testTemplate('negative isn\'t', {
		$keepIf: 'VAR isn\'t abc',
		xyz: 123,
	}, {
		VAR: 'abc'
	}, undefined);

	testTemplate('nested positive is', {
		abc: [
			{
				$keepIf: 'VAR is abc',
				xyz: 123,
			},
		],
	}, {
		VAR: 'abc'
	}, {
		abc: [
			{
				xyz: 123,
			}
		],
	});

	testTemplate('nested negative is', {
		abc: [
			{
				$keepIf: 'VAR is abc',
				xyz: 123,
			},
		],
		def: {
			$keepIf: 'VAR is abc',
			xyz: 123,
		},
	}, {}, {
		abc: [],
	});
});

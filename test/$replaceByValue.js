const { testTemplate } = require('./test');

describe('$replaceByValue', () => {
	testTemplate('simple', {
		$replaceByValue: 'abc',
	}, {}, 'abc');

	testTemplate('nested', {
		abc: [
			{
				$replaceByValue: {
					$replaceByValue: 'abc'
				},
			},
		],
	}, {}, {
		abc: [
			'abc',
		],
	});
});

const { testInvalidTemplate } = require('./test');

describe('$keepIf / $dropIf conflict', () => {
	testInvalidTemplate('simple', {
		$dropIf: 'VAR is abc',
		$keepIf: 'Var isn\'t abc',
	}, {});
	
	testInvalidTemplate('nested', {
		abc: [
			{
				$dropIf: 'VAR is abc',
				$keepIf: 'Var isn\'t abc',
			},
		],
	}, {});
});

describe('$replaceByValue / $replaceByVariable conflict', () => {
	testInvalidTemplate('simple', {
		$replaceByValue: 123,
		$replaceByVariable: 'abc',
	}, {});

	testInvalidTemplate('nested', {
		abc: [
			{
				$replaceByValue: 123,
				$replaceByVariable: 'abc',
			},
		],
	}, {});
});

describe('$replaceByValue / $map conflict', () => {
	testInvalidTemplate('simple', {
		$replaceByValue: 123,
		$map: 'var1',
	}, {});

	testInvalidTemplate('nested', {
		abc: [
			{
				$replaceByValue: 123,
				$map: 'var1',
			},
		],
	}, {});
});


describe('$replaceByVariable / $map conflict', () => {
	testInvalidTemplate('simple', {
		$replaceByValue: 'var1',
		$map: 'var2',
	}, {});

	testInvalidTemplate('nested', {
		abc: [
			{
				$replaceByValue: 'var1',
				$map: 'var2',
			},
		],
	}, {});
});

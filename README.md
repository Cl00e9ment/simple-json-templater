# Simple JSON Templater

Keeps, drops and replaces JSON value/key pairs in function of given variables.

## Installation

```bash
npm install simple-json-templater
```

## Templating Language Specifications

The template is a JSON object that can contains meta keys which are the following:

- $keepIf
- $dropIf
- $replaceByValue
- $replaceByVariable

The meta keys describe how to build the final JSON. All meta keys are removed from the final JSON.

If you want to use a key that start with a dollar you can escape it by adding another dollar. Example:

```json
{
  "abc": 123,
  "$$xyz": 456
}
```

gives:

```json
{
  "abc": 123,
  "$xyz": 456
}
```

### $replaceByValue

> Can't be used concurrently with $replaceByVariable or $map.

The parent object will be replaced by the value. This is helpful in combinaison with `$keepIf` and `$dropIf`.

Example:

```json
{
  "abc": [
    {
      "$replaceByValue": 123
    }
  ]
}
```

gives:

```json
{
  "abc": [
    123
  ]
}
```

### $replaceByVariable

> Can't be used concurrently with $replaceByValue or $map.

The parent object will be replaced by the variable.

Example with `var1 = "xyz"`:

```json
{
  "abc": {
    "$replaceByVariable": "var1"
  }
}
```

gives:

```json
{
  "abc": "xyz"
}
```

### $map

> Can't be used concurrently with $replaceByValue or $replaceByVariable.

Describe a variable wich its value will be maped by the `$case` meta keys. If no `$case` match the value, the `$default` meta key is used.

Example with `var1 = "def"`:

```json
{
  "xyz": {
    "$map": "var1",
    "$case:abc": 123,
    "$case:def": 456,
    "$default": 789
  }
}
```

gives:

```json
{
  "xyz": 456
}
```

Example with `var1 = "ghi"`:

```json
{
  "xyz": {
    "$map": "var1",
    "$case:abc": 123,
    "$case:def": 456,
    "$default": 789
  }
}
```

gives:

```json
{
  "xyz": 789
}
```

Example with `var1 = "ghi"` again but there is no `$default` key:

```json
{
  "xyz": {
    "$map": "var1",
    "$case:abc": 123,
    "$case:def": 456
  },
  "uvw": [
    {
      "$map": "var1",
      "$case:abc": 123,
      "$case:def": 456
    }
  ]
}
```

gives:

```json
{
  "uvw": [ null ]
}
```

### $keepIf

> Can't be used concurrently with $dropIf.

The parent object will be kept if and only if the expression is true.

Example with `var1 = "xyz"`:

```json
{
  "abc": {
    "$keepIf": "var1 is xyz",
    "$replaceByValue": 123
  },
  "def": {
    "$keepIf": "var1 isn't xyz",
    "$replaceByValue": 456
  }
}
```

gives:

```json
{
  "abc": 123
}
```

### $dropIf

> Can't be used concurrently with $keepIf.

The parent object will be removed if and only if the expression is true.

Example with `var1 = "xyz"`:

```json
{
  "abc": {
    "$dropIf": "var1 is xyz",
    "$replaceByValue": 123
  },
  "def": {
    "$dropIf": "var1 isn't xyz",
    "$replaceByValue": 456
  }
}
```

gives:

```json
{
  "def": 456
}
```

### expression

An expression is a string that can have the following shape:

- `<variable> is <value>`
- `<variable> isn't <value>`

Variable and value can't have space.

## Implementation

Import the `parse` function:

```js
// like this
const { parse } = require('simple-json-templater');

// or like this
import { parse } from 'simple-json-templater';
```

Then use it:

```js
/*

parse(
  template: { [key: string]: any },
  variables?: { [key: string]: string }
): any

*/

const result = parse({
  abc: {
    $replaceByVariable: 'var1'
  },
  xyz: [
    {
      $keepIf: 'var2 is keepFirst',
      $replaceByValue: 'first'
    },
    {
      $dropIf: 'var2 is keepFirst',
      $replaceByValue: 'second'
    }
  ]
}, {
  var1: '123',
  var2: 'keepSecond'
});

console.log(result);

/*

{
  abc: '123',
  xyz: [
    'second'
  ]
}

*/
```

## Misc

contact: [clem.saccoccio@protonmail.com](mailto:clem.saccoccio@protonmail.com)  
licence: [GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.txt)

![GPL3](https://www.gnu.org/graphics/gplv3-with-text-84x42.png)

// Type definitions for simple-json-templater
// Project: simple-json-templater
// Definitions by: Clément Saccoccio <https://c-saccoccio.fr>

export function parse(template: { [key: string]: any }, variables?: { [key: string]: string }): any;

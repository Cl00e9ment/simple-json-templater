// Copyright 2020 Clément Saccoccio

/*
	This file is part of JSON Simple Templater.

	JSON Simple Templater is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	JSON Simple Templater is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with JSON Simple Templater.  If not, see <https://www.gnu.org/licenses/>.
*/

import { isPresentAtMostOnce } from './utils';

const isCircular = require('is-circular');

interface AnyObject {
	[key: string]: any;
}

interface Variables {
	[key: string]: string;
}

const metaKeys = [
	'$keepIf',
	'$dropIf',
	'$replaceByValue',
	'$replaceByVariable',
	'$map',
	/^\$case:.*$/,
	'$default',
];

class SimpleJSONTemplater {

	private variables: Variables;

	public constructor(variables: Variables = {}) {
		if (typeof variables !== 'object') {
			throw 'variables must be an object';
		}
		for (const key in variables) {
			if (typeof variables[key] !== 'string') {
				throw 'variables must be of type { [key: string]: string }';
			}
		}
		this.variables = variables;
	}

	public parse(template: AnyObject): any {
		if (template === undefined || template === null) {
			throw 'template can\'t be undefined or null';
		}
		if (typeof template !== 'object') {
			throw 'template must be an object';
		}
		if (isCircular(template)) {
			throw 'template contains a circular reference';
		}
		return this.willObjectBeKept(template) ? this.parseObject(template) : undefined;
	}

	private parseObject(object: AnyObject): any {
		if (this.willObjectBeTransformed(object)) {
			return this.transformObject(object);
		}
		let newObject = {};
		for (const key in object) {
			if (this.isKeyMeta(key)) {
				continue;
			}
			let value = object[key];
			if (Array.isArray(value)) {
				value = this.parseArray(value);
			} else if (typeof value === 'object') {
				if (!this.willObjectBeKept(value)) {
					continue;
				}
				value = this.parseObject(value);
			}
			newObject[this.escapeKey(key)] = value;
		}
		return newObject;
	}

	private parseArray(array: any[]): any[] {
		const newArray = [];
		for (const element of array) {
			if (Array.isArray(element)) {
				newArray.push(this.parseArray(element));
			} else if (typeof element === 'object') {
				if (this.willObjectBeKept(element)) {
					newArray.push(this.parseObject(element));
				}
			} else {
				newArray.push(element);
			}
		}
		return newArray;
	}

	private transformObject(object: AnyObject): any {
		let replacement;
		if ('$replaceByValue' in object) {
			replacement = object.$replaceByValue;
		} else if ('$replaceByVariable' in object) {
			replacement = this.variables[object.$replaceByVariable];
		} else if ('$map' in object) {
			const value = this.variables[object.$map];
			for (const key in object) {
				if (key.match(/^\$case:(.*)$/)) {
					if (RegExp.$1 === value) {
						replacement = object[key];
						break;
					}
				} else if (key === '$default') {
					replacement = object[key];
				}
			}
		} else {
			throw 'internal error: could not transform object';
		}
		if (Array.isArray(replacement)) {
			return this.parseArray(replacement);
		} else if (typeof replacement === 'object') {
			if (this.willObjectBeKept(replacement)) {
				return this.parseObject(replacement);
			} else {
				return undefined;
			}
		} else {
			return replacement;
		}
	}

	private escapeKey(key: string): string {
		return key.startsWith('$$') ? key.substring(1) : key;
	}

	private isKeyMeta(key: string): boolean {
		if (key[0] !== '$' || key[1] === '$') {
			return false;
		}
		for (const metaKey of metaKeys) {
			if (metaKey instanceof RegExp) {
				if (key.match(metaKey)) {
					return true;
				}
			} else if (key === metaKey) {
				return true;
			}
		}
		throw `unknown meta key: ${key}`;
	}

	private willObjectBeKept(object: AnyObject): boolean {
		if ('$keepIf' in object && '$dropIf' in object) {
			throw '$keepIf and $dropIf can\'t be used concurrently';
		}
		if ('$keepIf' in object) {
			return this.evaluateExpression(object.$keepIf);
		}
		if ('$dropIf' in object) {
			return !this.evaluateExpression(object.$dropIf);
		}
		return true;
	}

	private willObjectBeTransformed(object: AnyObject): boolean {
		const replaceByValue = '$replaceByValue' in object;
		const replaceByVariable = '$replaceByVariable' in object;
		const map = '$map' in object;
		if (!isPresentAtMostOnce([replaceByValue, replaceByVariable, map], true)) {
			throw '$replaceByValue, $replaceByVariable and $map can\'t be used concurrently';
		}
		if (replaceByVariable && typeof object.$replaceByVariable !== 'string') {
			throw '$replaceByVariable must be of type string';
		}
		if (map && typeof object.$map !== 'string') {
			throw '$map must be of type string';
		}
		return replaceByValue || replaceByVariable || map;
	}

	private evaluateExpression(expression: string): boolean {
		if (typeof expression !== 'string') {
			throw 'expression must be of type string';
		}
		if (!expression.match(/^([^ ]*) +(is|isn't) +([^ ]*)$/)) {
			throw 'invalid expression';
		}
		const value = this.variables[RegExp.$1];
		return RegExp.$2 === 'is' ? value === RegExp.$3 : value !== RegExp.$3;
	}
}

export function parse(template: AnyObject, variables?: Variables): any {
	return new SimpleJSONTemplater(variables).parse(template);
}

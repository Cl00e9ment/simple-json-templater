// Copyright 2020 Clément Saccoccio

/*
	This file is part of JSON Simple Templater.

	JSON Simple Templater is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	JSON Simple Templater is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with JSON Simple Templater.  If not, see <https://www.gnu.org/licenses/>.
*/

export function isPresentAtMostOnce(haystack: any[], needle: any) {
	let found = false;
	for (const element of haystack) {
		if (element === needle) {
			if (found) {
				return false;
			} else {
				found = true;
			}
		}
	}
	return true;
}
